/*
=======================================================================
File Name : HelloAndroidWithAnimation.java 
Author : Javier A. Fresneda Rivera
University of Puerto Rico - Mayag�ez Campus
Description : First Assignment, Implement Hello Android!. 
This time I wanted to experiment a little so added various type of 
animations to the text, based in yet another tutorial.
=======================================================================
*/ 

package com.example.helloandroidwithanimation;

import android.app.Activity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

/**Hello Android! Assignment, this time with animations saved in external xmls
 * and a slightly modified layout.
 * @author Javier A. Fresneda Rivera
 */
public class HelloAndroidWithAnimationActivity extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }
    
    /**
     * runs the various animations
     */
    private void RunAnimations() {
    	
    	//first word using alpha animation
        Animation a = AnimationUtils.loadAnimation(this, R.anim.alpha);
        a.reset();
        TextView tv = (TextView) findViewById(R.id.firstTextView);
        tv.clearAnimation();
        tv.startAnimation(a);
     
    	//second word using translate animation
        a = AnimationUtils.loadAnimation(this, R.anim.translate);
        a.reset();
        tv = (TextView) findViewById(R.id.secondTextView);
        tv.clearAnimation();
        tv.startAnimation(a);
     
    	//third word using rotate animation

        a = AnimationUtils.loadAnimation(this, R.anim.rotate);
        a.reset();
        tv = (TextView) findViewById(R.id.thirdTextView);
        tv.clearAnimation();
        tv.startAnimation(a);
    }
    
    
    /* (non-Javadoc)
     * @see android.app.Activity#onTouchEvent(android.view.MotionEvent)
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            RunAnimations();
        }
        return true;
    }
}