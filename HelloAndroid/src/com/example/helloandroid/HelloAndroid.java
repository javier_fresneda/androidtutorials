/*
=======================================================================
File Name : HelloAndroid.java 
Author : Javier A. Fresneda Rivera
University of Puerto Rico - Mayag�ez Campus
Description : First Assignment, Implement Hello Android!.
=======================================================================
*/ 

package com.example.helloandroid;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;




/**Hello Android! Assignment
 * @author Javier A. Fresneda Rivera
 */
public class HelloAndroid extends Activity {
    /** Called when the activity is first created. */
	 @Override
	    public void onCreate(Bundle savedInstanceState) {
	       
	    	super.onCreate(savedInstanceState);
	    	    	
//	        setContentView(R.layout.main); //To use resource string
	        
	    	TextView tv = new TextView(this);
    	
	        tv.setText("Hello, Android");
	        
	        setContentView(tv);
	    }
}